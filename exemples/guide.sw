#et Tuxicoman
#es Jesuislibre.net
#ei Website: https://tuxicoman.jesuislibre.net
#ei Mastodon: tuxicoman@social.jesuislibre.net

A l'attention des frustrés du traitement de texte,

J'écris un nouveau logiciel d'écriture de document papier pour ordinateur nommé:

# SimpleWriter

#+
# Etat des lieux

Voici les difficultés que je rencontre dans les logiciels existants:

#* Sous __Word__ ou __LibreOffice__, obtenir un document cohérent est excessivement difficile. Les modfications du document sont erratiques, on ne comprend plus vraiment comment notre document est impacté quand on fait une modification et on passe des heures à chercher le bug dans la mise en page. Par exemple les ancres des images sautent continuellement et il faut beaucoup d'opérations manuelles pour les vérifier et replacer avant impression finale.

#* __LaTeX__ est trop complexe à prendre en main pour l'utilisateur et difficile à customiser.

#* __Markdown__ est trop simpliste et ne gère aucunement la mise en page des images.

Enfin les réglages de mise en page par défaut sont moches et il faut beaucoup de compétences pour avoir quelque chose de sympa.


# Problèmes à résoudre

Une chose importante est négligée par les éditeurs de texte moderne : **un gros travail de mise en page spécifique est nécessaire pour les documents papier**.

Par exemple:
#* s'assurer qu'un titre et le texte qui le suit ne soient pas séparés par un saut de page
#* s'assurer qu'une liste n'est pas coupée entre 2 pages et est accompagnée du texte que la précède
#* s'assurer qu'une image et l'élément qui la précède ne soient pas séparés par un saut de page
#* s'assurer de ne pas avoir un petit paragraphe de quelques mots tout seul en haut d'une page (sauf si il précède une image)

Concernant les espacements verticaux entre les blocs (paragraphe, titre, image...), on aimerait que ceux-ci soit des *garanties* et non des espaces toujours ajoutés.

Par exemple, il faut ménager un espacement entre un titre et le bloc qui le précède sur la même page. Mais si le titre se retrouve être le premier block à être écrit en haut d'une page, il ne faut pas lui ajouter d'espacement avant, il y a déja la marge de la page !

Pour les images, on aime souvent assurer des espacements autour et entre les images. ca devrait être le comportement par défaut !

Ensuite on veut que l'image rentre dans la page bien sûr. Et si on met plusieurs images côte à cote, qu'elles soient alignées sur leur bords haut ou bas.

# Solution
J'ai donc imaginé travailler dans un format explicite (des caractères spéciaux inclus dans le texte servent à donner des instructions de formatage) comme Markdown ou LaTeX et avoir une preview instantanée en PDF.

C'est possible en utilisant la bibliothèque __ReportLab__ qui permet de générer des PDFs en utilisant un concept de flow de blocks qui saute de frame en frame. Chaque frame étant l'espace d'affichage possible dans une page. Il a donc notion du saut de frame/page contraitement aux implémentation sur base de CSS pour le Web.

Le formatage serait un dérivé du Markdown dans l'esprit afin d'exposer le minimum d'instructions pour ne pas distraire l'utilisateur qui tape son texte.

Un avantage induit est qu'il est possible d'utiliser des logiciel de versionning (GIT, etc...) pour gérer les éditions de document à plusieurs.

Le code source se trouve à __https://framagit.org/tuxicoman/simplewriter__

# La preuve par l'exemple
Ce document a été réalisé par le logiciel SimpleWriter sans utiliser un seul saut de page manuel, contrôle de la longueur de paragraphe ou autre astuce de mise en page spécifique.

Pour créer le PDF, il faut lancer la commande : "python3 simplewriter.py guide.sw" qui génère un fichier "guide.pdf" dans le même dossier qu'où se trouve "guide.sw"
Le fichier "guide.sw" étant le fichier texte de formatage qui décrit ce présent guide !
La génération du PDF prend quelque millisecondes.

# Syntaxe

Voici un petit guide du formatage explicite à utiliser. Les " servent à encadrer le texte tapé et ne doivent pas être repoduits.

#+

# Paragraphe

Il suffit juste de taper votre texte.
Pour sauter de ligne , faire un retour de ligne.

Pour sauter de paragraphe, ajouter une ligne vide au minimum. Si il y a plusieurs lignes vides, ce n'est pas grave ! Ca ne comptera que comme un seul saut de paragraphe.

# Titre
Préfixer votre titre de "# ". Par exemple: "# Titre" pour faire comme ci dessus.
Attention : L'espace après le # est important! Si vous ne le mettez pas la ligne sera ignorée.

En utilisant "#+" et "#-" sur une ligne, vous pouvez faire monter descendre ou monter dans la hiérarchie les titres qui seront définis ensuite dans le document.
C'est pratique car vous pouvez ainsi facilement décaler d'un niveau de hiérarchie tout un pan de votre texte sans devoir modifier chaque titre.

# Liste
Préfixer votre liste de "#* ". Et rajouter autant de * que de sous niveaux.

Par exemple:
  #* ma liste
  #* a un niveau
  #** et un deuxième niveau

donnera:
#* ma liste
#* a un niveau
#** et un deuxième niveau
Attention : L'espace après le # est important! Si vous ne le mettez pas la ligne sera ignorée.

# Italique
Il faut ajouter le caractère "*" de chaque coté de la séquence de mots.
Par exemple: "ceci est *un bout de phrase* en italique"
Ce qui compte c'est qu'il y ait des caractères collés après le premier "*" et avant le dernier "*".

# Gras
Il faut ajouter "**" (2 étoiles) de chaque coté de la séquence de mots.
Par exemple: "ceci est **un bout de phrase** en gras"
Ce qui compte c'est qu'il y ait des caractères collés après le premier "**" et avant le dernier "**".

# Souligné
Il faut ajouter un "__" (2 underscores) de chaque coté de la séquence de mots.
Par exemple: "ceci est __un bout de phrase__ en souligné"
Ce qui compte c'est qu'il y ait des caractères collés après le premier "__" et avant le dernier "__".

# Image
La ligne doit commencer par "#i ", suivi de la taille en hauteur désirée, suivi d'une liste de chemin vers les images. Les chemins peuevent être un chemin relatif, absolu et même des adresses HTTP !

Par exemple, "#i 6.8 ./horizontal.jpg" va chercher l'image "horizontal.png" dans le dossier courant et l'afficher avec une hauteur de 6,8 cm :
#i 6.8 ./horizontal.jpg

Un autre exemple avec une URL web : "#i 20 https://tuxicoman.jesuislibre.net/blog/wp-content/uploads/Capture-decran-de-2021-06-21-12-08-52.pngg"
#i 20 https://tuxicoman.jesuislibre.net/blog/wp-content/uploads/Capture-decran-de-2021-06-21-12-08-52.png

Vous noterez que l'image est redimensionnée à une hauteur inférieure à celle demandée pour ne pas dépasser la largeur de la page.

Ainsi pour avoir une image pleine largeur il suffit d'indiquer une valeur de hauteur très grande.

Autre exemple avec plusieurs images : "#i 10 ./horizontal.jpg ./vertical.jpg"
#i 10 ./horizontal.jpg ./vertical.jpg
Lorsque les images sont précisées sur la même ligne, leur hauteur sont harmonisées.

Les images sont redimensionnées automatiquement pour que l'ensemble respecte la largeur de la page. Elles n'ont donc pas atteint les 10 cm demandés en hauteur.

# Entête

Une entête est définie par un titre "#et", un sous-titre "#es" et des infos "#ei".
Voici la syntaxe:

 #et Tuxicoman
 #es Jesuislibre.net
 #ei Website: http://tuxicoman.jesuislibre.net
 #ei Mastodon: tuxicoman@social.jesuislibre.net

Si vous définissez un de ces trois éléments, l'entête apparaitra au début du document. Vous n'êtes pas obligé de mettre cette information sur les premières ligne du fichier source.

# Date

Indique la date du jour.
Par exemple la syntaxe:
 #d
Va donner:
#d

#-

