#Copyright Tuxicoman
#You are allowed to use it and distribute it for free
#You are allowed to modify it and used your modifed version for free for your individual use.
#You cannot distribute modified versions. Please contribute and send me patches.

from reportlab.platypus import SimpleDocTemplate, Paragraph, Image, Table, KeepTogether, Flowable
from reportlab.lib.styles import StyleSheet1, ParagraphStyle, _baseFontName, _baseFontNameB, _baseFontNameBI
from reportlab.lib.enums import TA_JUSTIFY, TA_LEFT, TA_CENTER, TA_RIGHT
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch, cm
import sys, time,copy,re, os.path, os, locale
import datetime
locale.setlocale(locale.LC_ALL, locale.getlocale())

PAGE_HEIGHT=defaultPageSize[1]
PAGE_WIDTH=defaultPageSize[0]

from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.pdfmetrics import registerFontFamily
from reportlab.pdfbase.ttfonts import TTFont
import reportlab

pdfmetrics.registerFont(TTFont('Verdana', 'verdana.ttf'))
pdfmetrics.registerFont(TTFont('Verdanab', 'verdanab.ttf'))
pdfmetrics.registerFont(TTFont('Verdanai', 'verdanai.ttf'))
pdfmetrics.registerFont(TTFont('Verdanaz', 'verdanaz.ttf'))
registerFontFamily('Verdana',normal='Verdana',bold='Verdanab',italic='Verdanai',boldItalic='Verdanaz')

template_colors = { "blue" : "#0048cb"}
base_font_name = "Verdana"
def get_stylesheet():
  stylesheet = StyleSheet1()
  stylesheet.add(ParagraphStyle(name='Normal',
                                    fontName=base_font_name,
                                    fontSize=10,
                                    leading=15)
                     )

  stylesheet.add(ParagraphStyle(name='Paragraph',
                                    parent=stylesheet['Normal'],
                                    spaceBefore=10,
                                    spaceAfter=16,
                                    alignment=4)
                     )
  stylesheet.add(ParagraphStyle(name='Bullet',
                                    parent=stylesheet['Normal'],
                                    spaceBefore=2,
                                    spaceAfter=2,
                                    bulletIndent=0,
                                    alignment=4)
                     )
  stylesheet.add(ParagraphStyle(name='ParagraphToBullet',
                                    parent=stylesheet['Paragraph'],
                                    spaceAfter=0) #Bullet.spaceBefore will provide space
                     )

  stylesheet.add(ParagraphStyle(name='BulletToOther',
                                    parent=stylesheet['Bullet'],
                                    spaceAfter=stylesheet['Paragraph'].spaceAfter)
                     )

  stylesheet.add(ParagraphStyle(name='h0',
                                  parent=stylesheet['Normal'],
                                  fontName = base_font_name + "b",
                                  fontSize=18,
                                  leading=28,
                                  spaceBefore=30,
                                  spaceAfter=0,
                                  alignment=TA_CENTER,
                                  textColor=template_colors["blue"])
                      )

  stylesheet.add(ParagraphStyle(name='h1',
                                  parent=stylesheet['Normal'],
                                  fontName = base_font_name + "b",
                                  fontSize=13,
                                  leading=20,
                                  spaceBefore=30,
                                  spaceAfter=10,
                                  textColor=template_colors["blue"] )
                      )
  stylesheet.add(ParagraphStyle(name='h2',
                                  parent=stylesheet['Normal'],
                                  fontName = base_font_name + "b",
                                  fontSize=11,
                                  leading=12,
                                  spaceBefore=20,
                                  spaceAfter=4,
                                  textColor=template_colors["blue"])
                      )
  return stylesheet
styles = get_stylesheet()

def get_header_style(hierachical_level):
  if hierachical_level == 0:
    return styles["h0"]
  elif hierachical_level == 1:
    return styles["h1"]
  else:
    return styles["h2"]

def parse_markup(text):
  text = re.sub(r'(?!\w)\*\*(\w.*?\w)\*\*(?!\w)', '<b>\\1</b>', text) #bold
  text = re.sub(r'(?!\w)\*(\w.*?\w)\*(?!\w)', '<i>\\1</i>', text) #italic
  text = re.sub(r'(?![a-zA-Z0-9])__(\w.*?\w)__(?![a-zA-Z0-9_])', '<u>\\1</u>', text) #underline

  return text


def myLaterPages(canvas, doc):

    #Page number background
    canvas.saveState()
    canvas.setStrokeColor(template_colors["blue"])
    p = canvas.beginPath()
    right_side = PAGE_WIDTH- inch
    right_bottom = 0.7 * inch
    height = 0.2 * inch
    width = 0.5*inch
    p.moveTo(right_side, right_bottom)
    p.lineTo(right_side, right_bottom+height)
    canvas.drawPath(p, fill=False, stroke=True)
    canvas.restoreState()

    #Page number
    canvas.saveState()
    canvas.setFillColor(template_colors["blue"])
    canvas.setFont(_baseFontName,10)
    canvas.drawRightString(right_side-height/2, right_bottom+0.05*inch, "%d" % (doc.page))
    canvas.restoreState()


class Block:
  def __init__(self, block_type, text="", hierachical_level=0, flowable=None):
    self.block_type = block_type
    self.text = text
    self.hierachical_level = hierachical_level
    self.flowable = flowable

class Header(Paragraph):
  def __init__(self, text, style=None, bulletText = None, frags=None, caseSensitive=1, encoding='utf8', hierachical_level=0):
    if hierachical_level <= 1:
      pass#text = text.upper()
    Paragraph.__init__(self, text, style, bulletText, frags, caseSensitive, encoding)
    self.hierachical_level = hierachical_level

  def draw(self):
    if self.hierachical_level == 1:

      canvas = self.canv
      canvas.saveState()
      canvas.setStrokeColor(template_colors["blue"])

      margin_left = - 0.1 * inch
      width= 0.3*inch
      small_width= 0.1*inch
      padding_top = 5

      canvas.line(0,0,self.width,0)
      canvas.restoreState()

    Paragraph.draw(self)



class Entete(Flowable):
  def __init__(self, entete_dict):
    self.height = 100
    self.width = 100
    self.title = "\n".join(entete_dict["title"]).replace(" ", "&nbsp;")
    self.subtitle = "\n".join(entete_dict["subtitle"]).replace(" ", "&nbsp;")
    self.infos = "\n".join(entete_dict["infos"]).replace(" ", "&nbsp;")
    self.margin = 10
    self.spaceAfter = 80

    self.title_style = ParagraphStyle(name='title',
                                  parent=styles['Normal'],
                                  fontName = base_font_name,
                                  fontSize=20,
                                  leading=20,
                                  textColor=template_colors["blue"] )

    self.subtitle_style = ParagraphStyle(name='subtitle',
                                  parent=self.title_style,
                                  fontSize=10,
                                  leading=12, )

    self.infos_style = ParagraphStyle(name='infos',
                                  parent=self.subtitle_style,
                                  fontName = base_font_name,
                                  fontSize=8,
                                  leading=10, )

    self.widgets = []




  def wrap(self, availWidth, availHeight):
      self._setup(availWidth, availHeight)
      return (self.width, self.height)

  def _setup(self, availWidth, availHeight):
    self.widgets = []
    h_count = 2*self.margin

    P=Paragraph(self.title, style=self.title_style)
    w,h = P.wrap(availWidth=(availWidth-2*self.margin)/2, availHeight=availHeight-h_count)

    self.widgets.append([P, w,h])

    h_count+= h+self.margin/2

    P=Paragraph(self.subtitle, style=self.subtitle_style)
    w,h = P.wrap(availWidth=(availWidth-2*self.margin)/2, availHeight=availHeight-h_count)

    self.widgets.append([P, w,h])

    h_count+= h

    self.height = h_count

    #Other column

    h_count=2*self.margin


    P=Paragraph(self.infos.replace("\n", "<br />"), style=self.infos_style)
    w,h = P.wrap(availWidth=P.minWidth(), availHeight=availHeight-h_count)

    self.widgets.append([P, w,h])
    h_count += h

    self.height = max(self.height, h_count)
    self.width = availWidth

  def draw(self):
    canvas = self.canv

    P,w,h = self.widgets[0]
    offset = self.height-self.margin-h
    P.drawOn(canvas, self.margin, offset)


    P,w,h = self.widgets[1]
    offset -= h+self.margin/2
    P.drawOn(canvas, self.margin, offset)

    P,w,h = self.widgets[2]
    P.drawOn(canvas, self.width-self.margin-w, (self.height-h)/2)

    canvas.saveState()
    canvas.setStrokeColor(template_colors["blue"])
    p0x = self.width-self.margin-w-self.margin
    canvas.line(p0x, 0+self.margin, p0x, self.height-self.margin)
    canvas.restoreState()




class SimpleWriterDocument(SimpleDocTemplate):
  def __init__(self, filename, **kw):
    SimpleDocTemplate.__init__(self, filename, **kw)
    self.key_iterator = 0
    self.level_offset = None

  def afterFlowable(self, flowable):
    SimpleDocTemplate.afterFlowable(self, flowable)
    if isinstance(flowable, Header):
      key = "%i" % self.key_iterator
      self.canv.bookmarkPage(key)
      if self.level_offset is None:
        self.level_offset = flowable.hierachical_level #Offset to allow not using titles in upper levels
      self.canv.addOutlineEntry(key=key, title=flowable.text, level=flowable.hierachical_level-self.level_offset)
      self.key_iterator +=1


def go(content, destination):
    doc_leftMargin = doc_rightMargin = 1 * inch
    Story = []

    doc_title = ""
    last_line_type = "blank"
    hierachical_level = 0

    #Collect unique document blocks
    entete_dict = {"title":[], "subtitle":[], "infos":[]}

    for line in content:
      line = line.rstrip()
      if len(line) == 0:
        line_type = "blank"
      elif line[0] == '#' and len(line) >= 2:
        if line[1] == "+":
          hierachical_level +=1
        elif line[1] == "-":
          hierachical_level -=1
          assert(hierachical_level >=0)
        elif line[1] == " ":
          line_type = "header"

          if last_line_type == "header":
            assert(Story[-1].block_type == line_type)
            assert(Story[-1].hierachical_level == hierachical_level)
            Story[-1].text +=+"<br />"+line[2:]
          else:
            p = Block(block_type=line_type, text=line[2:], hierachical_level=hierachical_level)
            Story.append(p)

          if doc_title == "" and hierachical_level == 0:
            doc_title = line[2:]
        elif line[1] == "i" and len(line) >= 3:
          line_type = "image"
          image_args = line[3:].split(" ")
          if len(image_args) >=2:
            max_height = image_args[0]
            max_height = float(max_height) * cm
            image_paths = image_args[1:]
            if len(image_paths) == 1:
              p = Image(image_paths[0])
              p._restrictSize(PAGE_WIDTH - (doc_rightMargin + doc_leftMargin), max_height)
              p.spaceAfter = p.spaceBefore = 20
            else:
              images = [[Image(image_path) for image_path in image_paths]]

              for row in images:
                image_ratios  = [image.drawWidth/image.drawHeight for image in row]
                width_sum_ratio = sum(image_ratios)
                max_width = PAGE_WIDTH - (doc_rightMargin + doc_leftMargin) - (len(row)-1) * 20
                max_height = min(max_width/width_sum_ratio, 4 * inch)

                for image_ratio, image in zip(image_ratios, row):
                  image.drawWidth = image_ratio * max_height
                  image.drawHeight = max_height

              p = Table(images)
            p = Block(block_type=line_type, flowable=p)
            Story.append(p)
        elif line[1] == "*" and len(line) >= 3:
          indentation_level = 0
          i = 2
          while True:
            if line[i] == "*":
              indentation_level+=1
            else:
              break
            i+=1
          line_type = "bullet"
          #Intra markup (should be regexp to handle case where surrounding is not space but not a char)
          text_markup = parse_markup(line[i:])

          p = Block(block_type=line_type, text=text_markup, hierachical_level=indentation_level)
          Story.append(p)

        elif line[1] == "e" and len(line) >= 4:
          line_type = "entete"
          if line[2] == "t":
            entete_dict["title"].append(line[4:])
          elif line[2] == "s":
            line_type = "entete_subtitle"
            entete_dict["subtitle"].append(line[4:])
          elif line[2] == "i":
            line_type = "entete_info"
            entete_dict["infos"].append(line[4:])

        elif line[1] == "d":
          line_type = "date"
          p = Block(block_type=line_type, text="Le {dt.day} {dt:%B} {dt.year}".format(dt=datetime.datetime.now()))
          Story.append(p)
        else:
          line_type="blank"


      else:
        line_type = "paragraph"
        #Intra markup (should be regexp to handle case where surrounding is not space but not a char)
        text_markup = parse_markup(line)
        if last_line_type == "paragraph":
          assert(Story[-1].block_type == line_type)
          Story[-1].text += "<br />"+text_markup
        else:
          p = Block(block_type=line_type, text=text_markup)
          Story.append(p)

      last_line_type = line_type

    new_story = []
    keep_together = []
    paragraph_glue = 0
    index = len(Story)
    for block in reversed(Story): #Iterating backward is easier for the glue algo

      index-=1

      #Previous block
      if index != 0: #Not the last one
        upper_block = Story[index-1]
      else:
        upper_block = None

      if index != len(Story)-1: #Not the last one
        lower_block = Story[index+1]
      else:
        lower_block = None

      assert(isinstance(block, Block))
      if block.block_type == "header":
        style = get_header_style(block.hierachical_level)
        p = Header(block.text, style, hierachical_level=block.hierachical_level)
      elif block.block_type == "bullet":
        if lower_block is not None and lower_block.block_type != "bullet":
          bullet_style = styles["BulletToOther"]
        else:
          bullet_style = styles["Bullet"]
        bullet_style = copy.copy(bullet_style)
        bullet_style.bulletIndent = 20*block.hierachical_level
        p = Paragraph(block.text, bullet_style, bulletText="•")
      elif block.block_type == "paragraph":
        if lower_block is not None and lower_block.block_type == "bullet":
          style = styles["ParagraphToBullet"]
        else:
          style = styles["Paragraph"]
        p = Paragraph(block.text, style )
      elif block.block_type == "date":
        style = styles["Paragraph"]
        p = Paragraph(block.text, style )
      elif block.block_type == "image":
        p = block.flowable

      else:
        assert(False) #Not supported block yet

      #Keep or not together?
      glue_with_upper = False
      if upper_block is not None:
        if upper_block.block_type == "header":
          glue_with_upper = True
        elif block.block_type == "image" and upper_block.block_type != "image":
          glue_with_upper = True
        elif block.block_type == "bullet":
          glue_with_upper = True
        elif block.block_type == "paragraph" and (lower_block is None or lower_block.block_type not in ("image", "bullet")):
          if paragraph_glue + len(block.text) <300:
            paragraph_glue = +len(block.text)
            glue_with_upper = True

      if not glue_with_upper:
        paragraph_glue = 0

      if glue_with_upper:
        keep_together.insert(0,p)
      elif len(keep_together) > 0:
        keep_together.insert(0,p)
        new_story.append(KeepTogether(keep_together.copy()))
        keep_together.clear()
        paragraph_glue = 0
      else:
        new_story.append(p)

    new_story = new_story[::-1] #Reverse again

    #Collect unique document blocks
    if any(len(value) for value in entete_dict.values()):
      new_story.insert(0,Entete(entete_dict))


    doc = SimpleWriterDocument(destination, topMargin=0.7*inch , author="SimpleWriter", title=doc_title)
    doc.build(new_story, onFirstPage=myLaterPages, onLaterPages=myLaterPages)

if __name__=="__main__":
    if len(sys.argv) < 2:
      print("Missing path to source file")
      sys.exit()

    t_start = time.time() #Timer for performance statistics

    source_filename = sys.argv[1]
    with open(source_filename,"r") as f:
      content = f.readlines()
    destination = os.path.abspath(os.path.splitext(source_filename)[0] + ".pdf") #Decide where would be the output

    os.chdir(os.path.dirname(source_filename)) #Resolve future relative path from the source file location

    go(content, destination)

    print("generated in %.3fs" % (time.time()-t_start))
